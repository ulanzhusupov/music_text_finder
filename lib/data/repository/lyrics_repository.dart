import 'package:music_text_finder/core/network/dio_settings.dart';
import 'package:music_text_finder/data/models/lyrics_model.dart';

class LyricsRepository {
  Future<LyricsModel> getSongLyrics(String title, String artist) async {
    final response =
        await DioSettings().dio.get("https://api.lyrics.ovh/v1/$artist/$title");

    return LyricsModel.fromJson(response.data);
  }
}
