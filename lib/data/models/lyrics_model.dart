class LyricsModel {
  String? lyrics;

  LyricsModel({this.lyrics});

  LyricsModel.fromJson(Map<String, dynamic> json) {
    lyrics = json['lyrics'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['lyrics'] = lyrics;
    return data;
  }
}
