import 'dart:async';
import 'package:flutter/material.dart';
import 'package:music_text_finder/data/models/lyrics_model.dart';
import 'package:music_text_finder/data/repository/lyrics_repository.dart';
import 'package:music_text_finder/presentation/widgets/song_image.dart';
import 'package:music_text_finder/presentation/widgets/song_lyrics.dart';
import 'package:nowplaying/nowplaying.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  NowPlayingTrack? _currentTrack;
  LyricsModel? lyricssModel;
  ImageProvider? songImage;

  @override
  void initState() {
    super.initState();
    NowPlaying.instance.isEnabled().then((bool isEnabled) async {
      if (!isEnabled) {
        final shown = await NowPlaying.instance.requestPermissions();
        print('MANAGED TO SHOW PERMS PAGE: $shown');
      }
    });
  }

  Future<void> updateCurrentTrack(NowPlayingTrack? song) async {
    try {
      lyricssModel ??= await LyricsRepository()
          .getSongLyrics(song?.title ?? "", song?.artist ?? "");
    } catch (e) {
      lyricssModel = LyricsModel(lyrics: e.toString());
    }
    _currentTrack = song;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        child: StreamBuilder<NowPlayingTrack>(
          stream: NowPlaying.instance.stream,
          initialData: NowPlayingTrack.loading,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.active) {
              print("STATTEEEEEE: ${snapshot.connectionState}");
              print("MUSSSIIIICAAA: ${snapshot.data?.title}");
              if (_currentTrack?.title != snapshot.data?.title ||
                  _currentTrack?.artist != snapshot.data?.artist) {
                lyricssModel = null;
                WidgetsBinding.instance.addPostFrameCallback(
                    (_) => updateCurrentTrack(snapshot.data));
              }

              return CustomScrollView(
                slivers: [
                  SliverAppBar(
                    pinned: true,
                    expandedHeight: 457,
                    backgroundColor: Colors.black,
                    flexibleSpace: FlexibleSpaceBar(
                      expandedTitleScale: 1.4,
                      background: SongImage(
                        track: _currentTrack,
                        imageH: 20,
                        imageW: 20,
                      ),
                      titlePadding: const EdgeInsets.symmetric(
                        horizontal: 0,
                        vertical: 0,
                      ),
                      title: Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 8),
                        color: const Color.fromRGBO(0, 0, 0, 0.4),
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          "${_currentTrack?.title} - ${_currentTrack?.artist}",
                          style: const TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: AnimatedContainer(
                      duration: const Duration(seconds: 1),
                      padding: const EdgeInsets.all(16.0),
                      color: lyricssModel != null ? Colors.white : Colors.black,
                      child: lyricssModel != null
                          ? SongLyrics(
                              lyricsModel: lyricssModel,
                            )
                          : const Center(
                              child: Text(
                                "Lyrics request error",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                    ),
                  ),
                ],
              );
            }

            return const Center(
              child: Text(
                "Включите музыку в стороннем приложении",
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.black,
                ),
                textAlign: TextAlign.center,
              ),
            );
          },
        ),
      ),
    );
  }
}
