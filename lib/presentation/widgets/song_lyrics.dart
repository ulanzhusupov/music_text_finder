import 'package:flutter/material.dart';
import 'package:music_text_finder/data/models/lyrics_model.dart';

class SongLyrics extends StatelessWidget {
  const SongLyrics({super.key, required this.lyricsModel});
  final LyricsModel? lyricsModel;

  @override
  Widget build(BuildContext context) {
    List<String> listLyrics = lyricsModel?.lyrics?.split("\n") ?? ["empty"];
    listLyrics.removeAt(0);

    String formattedLyrics = listLyrics.join("\n");

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Text(
        formattedLyrics,
        style: const TextStyle(
          fontSize: 17,
          fontWeight: FontWeight.w400,
        ),
      ),
    );
  }
}
