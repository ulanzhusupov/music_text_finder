import 'dart:math';

import 'package:flutter/material.dart';
import 'package:nowplaying/nowplaying.dart';

class SongImage extends StatelessWidget {
  const SongImage({
    super.key,
    required this.track,
    required this.imageW,
    required this.imageH,
  });

  final NowPlayingTrack? track;
  final double imageW;
  final double imageH;

  @override
  Widget build(BuildContext context) {
    if (track != null) {
      return Image(
        key: Key(Random().nextInt(1000).toString()),
        image: track?.image ?? const NetworkImage(""),
        width: imageW,
        height: imageH,
        fit: BoxFit.none,
        errorBuilder: (context, error, stackTrace) {
          return const Center(
            child: Text(
              "No image",
              style: TextStyle(color: Colors.white),
            ),
          );
        },
      );
    }

    // if (track?.isResolvingImage ?? false) {
    //   return const SizedBox(
    //       width: 50.0,
    //       height: 50.0,
    //       child: CircularProgressIndicator(
    //           valueColor: AlwaysStoppedAnimation<Color>(Colors.white)));
    // }

    return const Center(
      child: Text('NO\nARTWORK\nFOUND',
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 12, color: Colors.white)),
    );
  }
}
