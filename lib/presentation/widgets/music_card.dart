import 'dart:math';

import 'package:flutter/material.dart';
import 'package:music_text_finder/presentation/widgets/song_image.dart';
import 'package:nowplaying/nowplaying.dart';

class MusicCard extends StatelessWidget {
  const MusicCard({
    super.key,
    required NowPlayingTrack? currentTrack,
  }) : _currentTrack = currentTrack;

  final NowPlayingTrack? _currentTrack;

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;

    return Container(
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 218, 218, 218),
        borderRadius: BorderRadius.all(
          Radius.circular(16),
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            width: 100,
            height: 100,
            child: SongImage(
                key:
                    Key(_currentTrack?.id ?? Random().nextInt(1000).toString()),
                track: _currentTrack,
                // songImage: songImage,
                imageW: 100,
                imageH: 100),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  width: screenWidth * 0.6,
                  child: Text(
                    _currentTrack?.title ?? "Empty",
                    style: const TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                const SizedBox(height: 15),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
